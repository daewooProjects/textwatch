﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Threading;
using System.Windows.Threading;
using System.Windows.Resources;
using System.Runtime.InteropServices;
using System.Diagnostics;
using Microsoft.Win32;

namespace textwatch
{
    /// <summary>
    /// Логика взаимодействия для gadget.xaml
    /// </summary>
    public partial class gadget : Window
    {
        [DllImport("user32.dll")]
        public static extern IntPtr GetForegroundWindow();
        public DispatcherTimer timer = new DispatcherTimer();
        private DispatcherTimer timerShowWindow = new DispatcherTimer() { Interval = TimeSpan.FromMilliseconds(100) }; 
        public gadget()
        {
            InitializeComponent();
            timer.Interval = TimeSpan.FromSeconds(60);
            timer.Tick += timer_Tick;
            timer.Start();
            //Keep window open
            timerShowWindow.Tick += (o, e) =>
            {
                 if (Process.GetCurrentProcess().MainWindowHandle != GetForegroundWindow())
            {
                this.WindowState = System.Windows.WindowState.Normal;
                //this.Activate();
                 }
            };
            timerShowWindow.IsEnabled = true;
            timerShowWindow.Start();
        }
        

        void timer_Tick(object sender, EventArgs e)
        {
            BrushIt();

        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            
            int ScreenWidth = System.Windows.Forms.Screen.FromHandle(new System.Windows.Interop.WindowInteropHelper(App.Current.MainWindow).Handle).WorkingArea.Width;
            this.Left = ScreenWidth - this.Width;
            
           
            timer.Start();
            BrushIt();

            string ExePath = System.Windows.Forms.Application.ExecutablePath;
            RegistryKey reg = Registry.CurrentUser.CreateSubKey("Software\\Microsoft\\Windows\\CurrentVersion\\Run\\");
            reg.SetValue("Text Watch", ExePath);
            reg.Close();
        }
        /*
        private void button1_Click(object sender, EventArgs e)
        {
            string ExePath = System.Windows.Forms.Application.ExecutablePath;
            RegistryKey reg = Registry.CurrentUser.CreateSubKey("Software\\Microsoft\\Windows\\CurrentVersion\\Run\\");
            reg.SetValue("MyTestApplication", "C:\\Users\\Max\\Desktop\\Всякое\\ЗАКОНЧЕННЫЕ РАБОТЫ\\Sortir\\Sortir\\bin\\Debug\\Sortir.exe");
            reg.Close();
        }*/

        private void RefreshAll() {
            //MessageBox.Show("Time to refresh");
                lblAnother.Foreground = Brushes.Black;
                lblAnother.Foreground = Brushes.Black;
                lblAnother1.Foreground = Brushes.Black;
                lblAnother2.Foreground = Brushes.Black;
                lblAnother5.Foreground = Brushes.Black;
                lblAnother6.Foreground = Brushes.Black;
                lblAnother7.Foreground = Brushes.Black;
                lblAnother8.Foreground = Brushes.Black;
                lblAnother9.Foreground = Brushes.Black;
                lblEight.Foreground = Brushes.Black;
                lblEleven.Foreground = Brushes.Black;
                lblFive.Foreground = Brushes.Black;
                lblForty.Foreground = Brushes.Black;
                lblFour.Foreground = Brushes.Black;
                lblHour.Foreground = Brushes.Black;
                lblHours.Foreground = Brushes.Black;
                lblMin.Foreground = Brushes.Black;
                lblMinFifteen.Foreground = Brushes.Black;
                lblMinFive.Foreground = Brushes.Black;
                lblMinFiveThirty.Foreground = Brushes.Black;
                lblMinTen.Foreground = Brushes.Black;
                lblNine.Foreground = Brushes.Black;
                lblOne.Foreground = Brushes.Black;
                lblSeven.Foreground = Brushes.Black;
                lblSix.Foreground = Brushes.Black;
                lblTen.Foreground = Brushes.Black;
                lblThirty.Foreground = Brushes.Black;
                lblThree.Foreground = Brushes.Black;
                lblTwelve.Foreground = Brushes.Black;
                lblTwenty.Foreground = Brushes.Black;
                lblTwo.Foreground = Brushes.Black;
                lblVinFive.Foreground = Brushes.Black;
                lblWithOut.Foreground = Brushes.Black;
        }

        
        private void BrushIt() {
            App.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
             {

                RefreshAll();
                int Hour = DateTime.Now.Hour;
                int Min = DateTime.Now.Minute;
                if (Hour > 12) Hour -= 12;
              
                
                if (Min <= 2) {
                    if (Hour <= 4)
                    {
                        lblHour.Content = "ЧАСА";
                        lblHour.Foreground = Brushes.WhiteSmoke;
                    }
                    else
                    {
                        lblHour.Content = "ЧАСОВ";
                        lblHour.Foreground = Brushes.WhiteSmoke;
                    }
                    switch (Hour)
                    {
                        case 1:
                            lblOne.Foreground = Brushes.WhiteSmoke;
                            break;
                        case 2:

                            lblTwo.Foreground = Foreground = Brushes.WhiteSmoke;
                            break;
                        case 3:
                            lblThree.Foreground = Brushes.WhiteSmoke;
                            break;
                        case 4:
                            lblFour.Foreground = Brushes.WhiteSmoke;
                            break;
                        case 5:
                            lblFive.Foreground = Brushes.WhiteSmoke;
                            break;
                        case 6:
                            lblSix.Foreground = Brushes.WhiteSmoke;
                            break;
                        case 7:
                            lblSeven.Foreground = Brushes.WhiteSmoke;
                            break;
                        case 8:
                            lblEight.Foreground = Brushes.WhiteSmoke;
                            break;
                        case 9:
                            lblNine.Foreground = Brushes.WhiteSmoke;
                            break;
                        case 10:
                            lblTen.Foreground = Brushes.WhiteSmoke;
                            break;
                        case 11:
                            lblEleven.Foreground = Brushes.WhiteSmoke;
                            break;
                        case 12:
                            lblTwelve.Foreground = Brushes.WhiteSmoke;
                            break;
                        default:
                            break;
                    }
                }
                else if (Min > 58)
                {
                    if (Hour <= 4)
                    {
                        lblHour.Content = "ЧАСА";
                        lblHour.Foreground = Brushes.WhiteSmoke;
                    }
                    else
                    {
                        lblHour.Content = "ЧАСОВ";
                        lblHour.Foreground = Brushes.WhiteSmoke;
                    }

                    switch (Hour)
                    {
                        case 1:
                            lblTwo.Foreground = Brushes.WhiteSmoke;
                            break;
                        case 2:
                            lblThree.Foreground = Foreground = Brushes.WhiteSmoke;
                            break;
                        case 3:
                            lblFour.Foreground = Brushes.WhiteSmoke;
                            break;
                        case 4:
                            lblFive.Foreground = Brushes.WhiteSmoke;
                            break;
                        case 5:
                            lblSix.Foreground = Brushes.WhiteSmoke;
                            break;
                        case 6:
                            lblSeven.Foreground = Brushes.WhiteSmoke;
                            break;
                        case 7:
                            lblEight.Foreground = Brushes.WhiteSmoke;
                            break;
                        case 8:
                            lblNine.Foreground = Brushes.WhiteSmoke;
                            break;
                        case 9:
                            lblTen.Foreground = Brushes.WhiteSmoke;
                            break;
                        case 10:
                            lblEleven.Foreground = Brushes.WhiteSmoke;
                            break;
                        case 11:
                            lblTwelve.Foreground = Brushes.WhiteSmoke;
                            break;
                        case 12:
                            lblHour.Foreground = Brushes.WhiteSmoke;
                            break;
                        default:
                            break;
                    }
                }
                else if (Min > 2 && Min < 43)
                {
                    switch (Hour)
                    {
                        case 1:
                            lblHour.Foreground = Brushes.WhiteSmoke;
                            break;
                        case 2:
                            lblTwo.Foreground = Foreground = Brushes.WhiteSmoke;
                            break;
                        case 3:
                            lblThree.Foreground = Brushes.WhiteSmoke;
                            break;
                        case 4:
                            lblFour.Foreground = Brushes.WhiteSmoke;
                            break;
                        case 5:
                            lblFive.Foreground = Brushes.WhiteSmoke;
                            break;
                        case 6:
                            lblSix.Foreground = Brushes.WhiteSmoke;
                            break;
                        case 7:
                            lblSeven.Foreground = Brushes.WhiteSmoke;
                            break;
                        case 8:
                            lblEight.Foreground = Brushes.WhiteSmoke;
                            break;
                        case 9:
                            lblNine.Foreground = Brushes.WhiteSmoke;
                            break;
                        case 10:
                            lblTen.Foreground = Brushes.WhiteSmoke;
                            break;
                        case 11:
                            lblEleven.Foreground = Brushes.WhiteSmoke;
                            break;
                        case 12:
                            lblTwelve.Foreground = Brushes.WhiteSmoke;
                            break;
                        default:
                            break;
                    }
                    if (Hour <= 4)
                    {
                        lblHour.Content = "ЧАСА";
                        lblHour.Foreground = Brushes.WhiteSmoke;
                    }
                    else
                    {
                        lblHour.Content = "ЧАСОВ";
                        lblHour.Foreground = Brushes.WhiteSmoke;
                    }

                    if (Min < 7 && Min > 2)
                    {
                        lblMinFive.Foreground = Brushes.WhiteSmoke;
                        lblMin.Foreground = Brushes.WhiteSmoke;
                    }
                    else if (Min > 6 && Min < 12)
                    {
                        lblMinTen.Foreground = Brushes.WhiteSmoke;
                        lblMin.Foreground = Brushes.WhiteSmoke;
                    }
                    else if (Min > 11 && Min < 17)
                    {
                        lblMinFifteen.Foreground = Brushes.WhiteSmoke;
                        lblMin.Foreground = Brushes.WhiteSmoke;
                    }
                    else if (Min > 16 && Min < 22)
                    {
                        lblTwenty.Foreground = Brushes.WhiteSmoke;
                        lblMin.Foreground = Brushes.WhiteSmoke;
                    }
                    else if (Min > 21 && Min < 27)
                    {
                        lblTwenty.Foreground = Brushes.WhiteSmoke;
                        lblMin.Foreground = Brushes.WhiteSmoke;
                        lblVinFive.Foreground = Brushes.WhiteSmoke;
                    }
                    else if (Min > 26 && Min < 32)
                    {
                        lblThirty.Foreground = Brushes.WhiteSmoke;
                        lblMin.Foreground = Brushes.WhiteSmoke;
                    }
                    else if (Min > 31 && Min < 37)
                    {
                        lblThirty.Foreground = Brushes.WhiteSmoke;
                        lblMinFiveThirty.Foreground = Brushes.WhiteSmoke;
                        lblMin.Foreground = Brushes.WhiteSmoke;
                    }
                    else if (Min > 36 && Min < 43)
                    {
                        lblForty.Foreground = Brushes.WhiteSmoke;
                        lblMin.Foreground = Brushes.WhiteSmoke;
                    }
                }
                else if (Min > 42 && Min < 58)
                {

                    if (Min > 42 && Min < 47)
                    {
                        lblWithOut.Foreground = Brushes.WhiteSmoke;
                        lblMinFifteen.Foreground = Brushes.WhiteSmoke;
                        lblMinFifteen.Content = "ПЯТНАДЦАТИ";
                        switch (Hour)
                        {
                            case 1:
                                lblMinFive.Foreground = Brushes.WhiteSmoke;
                                lblMinFive.Content = "ОДИН";
                                break;
                            case 2:
                                lblAnother2.Content = "ДВА";
                                lblAnother2.Foreground = Foreground = Brushes.WhiteSmoke;
                                break;
                            case 3:
                                lblAnother2.Content = "ТРИ";
                                lblAnother2.Foreground = Brushes.WhiteSmoke;
                                break;
                            case 4:
                                lblMinFiveThirty.Content = "ЧЕТЫ";
                                lblAnother7.Content = "РЕ";
                                lblMinFiveThirty.Foreground = Brushes.WhiteSmoke;
                                lblAnother7.Foreground = Brushes.WhiteSmoke;
                                break;
                            case 5:
                                lblMinFive.Foreground = Brushes.WhiteSmoke;
                                lblMinFive.Content = "ПЯТЬ";
                                break;
                            case 6:
                                lblForty.Foreground = Brushes.WhiteSmoke;
                                lblForty.Content = "ШЕСТЬ";
                                break;
                            case 7:
                                lblMinFive.Foreground = Brushes.WhiteSmoke;
                                lblMinFive.Content = "СЕМЬ";
                                break;
                            case 8:
                                lblMinFiveThirty.Foreground = Brushes.WhiteSmoke;
                                lblAnother7.Foreground = Brushes.WhiteSmoke;
                                lblMinFiveThirty.Content = "ВОСЕ";
                                lblAnother7.Content = "МЬ";
                                break;
                            case 9:
                                lblMinFiveThirty.Foreground = Brushes.WhiteSmoke;
                                lblAnother7.Foreground = Brushes.WhiteSmoke;
                                lblMinFiveThirty.Content = "ДЕВЯ";
                                lblAnother7.Content = "ТЬ";
                                break;
                            case 10:
                                lblMinFiveThirty.Foreground = Brushes.WhiteSmoke;
                                lblAnother7.Foreground = Brushes.WhiteSmoke;
                                lblMinFiveThirty.Content = "ДЕСЯ";
                                lblAnother7.Content = "ТЬ";
                                break;
                            case 11:
                                lblEleven.Foreground = Brushes.WhiteSmoke;
                                break;
                            case 12: 
                                lblTwelve.Foreground = Brushes.WhiteSmoke;
                                break;
                            default:
                                break;
                        }
                    }
                    else if (Min > 46 && Min < 53)
                    {
                        lblWithOut.Foreground = Brushes.WhiteSmoke;
                        lblTen.Foreground = Brushes.WhiteSmoke;
                        lblTen.Content = "ДЕСЯТИ";
                        switch (Hour)
                        {
                            case 1:
                                lblMinFive.Foreground = Brushes.WhiteSmoke;
                                lblMinFive.Content = "ОДИН";
                                break;
                            case 2:
                                lblAnother2.Content = "ДВА";
                                lblAnother2.Foreground = Foreground = Brushes.WhiteSmoke;
                                break;
                            case 3:
                                lblAnother2.Content = "ТРИ";
                                lblAnother2.Foreground = Brushes.WhiteSmoke;
                                break;
                            case 4:
                                lblMinFiveThirty.Content = "ЧЕТЫ";
                                lblAnother7.Content = "РЕ";
                                lblMinFiveThirty.Foreground = Brushes.WhiteSmoke;
                                lblAnother7.Foreground = Brushes.WhiteSmoke;
                                break;
                            case 5:
                                lblMinFive.Foreground = Brushes.WhiteSmoke;
                                lblMinFive.Content = "ПЯТЬ";
                                break;
                            case 6:
                                lblForty.Foreground = Brushes.WhiteSmoke;
                                lblForty.Content = "ШЕСТЬ";
                                break;
                            case 7:
                                lblMinFive.Foreground = Brushes.WhiteSmoke;
                                lblMinFive.Content = "СЕМЬ";
                                break;
                            case 8:
                                lblMinFiveThirty.Foreground = Brushes.WhiteSmoke;
                                lblAnother7.Foreground = Brushes.WhiteSmoke;
                                lblMinFiveThirty.Content = "ВОСЕ";
                                lblAnother7.Content = "МЬ";
                                break;
                            case 9:
                                lblMinFiveThirty.Foreground = Brushes.WhiteSmoke;
                                lblAnother7.Foreground = Brushes.WhiteSmoke;
                                lblMinFiveThirty.Content = "ДЕВЯ";
                                lblAnother7.Content = "ТЬ";
                                break;
                            case 10:
                                lblMinFiveThirty.Foreground = Brushes.WhiteSmoke;
                                lblAnother7.Foreground = Brushes.WhiteSmoke;
                                lblMinFiveThirty.Content = "ДЕСЯ";
                                lblAnother7.Content = "ТЬ";
                                break;
                            case 11:
                                lblEleven.Foreground = Brushes.WhiteSmoke;
                                break;
                            case 12: 
                                lblTwelve.Foreground = Brushes.WhiteSmoke;
                                break;
                            default:
                                break;
                        }
                    }
                    else if (Min > 51 && Min < 58)
                    {
                        lblFive.Foreground = Brushes.WhiteSmoke;
                        lblFive.Content = "ПЯТИ";
                        lblWithOut.Foreground = Brushes.WhiteSmoke;

                        switch (Hour)
                        {
                            case 1:
                                lblMinFive.Foreground = Brushes.WhiteSmoke;
                                lblMinFive.Content = "ОДИН";
                                lblMinFive.FontSize = 14;
                                break;
                            case 2:
                                lblAnother2.Content = "ДВА";
                                lblAnother2.Foreground = Foreground = Brushes.WhiteSmoke;
                                break;
                            case 3:
                                lblAnother2.Content = "ТРИ";
                                lblAnother2.Foreground = Brushes.WhiteSmoke;
                                break;
                            case 4:
                                lblMinFiveThirty.Content = "ЧЕТЫ";
                                lblAnother7.Content = "РЕ";
                                lblMinFiveThirty.Foreground = Brushes.WhiteSmoke;
                                lblAnother7.Foreground = Brushes.WhiteSmoke;
                                break;
                            case 5:
                                lblMinFive.Foreground = Brushes.WhiteSmoke;
                                lblMinFive.Content = "ПЯТЬ";
                                break;
                            case 6:
                                lblForty.Foreground = Brushes.WhiteSmoke;
                                lblForty.Content = "ШЕСТЬ";
                                break;
                            case 7:
                                lblMinFive.Foreground = Brushes.WhiteSmoke;
                                lblMinFive.Content = "СЕМЬ";
                                break;
                            case 8:
                                lblMinFiveThirty.Foreground = Brushes.WhiteSmoke;
                                lblAnother7.Foreground = Brushes.WhiteSmoke;
                                lblMinFiveThirty.Content = "ВОСЕ";
                                lblAnother7.Content = "МЬ";
                                break;
                            case 9:
                                lblMinFiveThirty.Foreground = Brushes.WhiteSmoke;
                                lblAnother7.Foreground = Brushes.WhiteSmoke;
                                lblMinFiveThirty.Content = "ДЕВЯ";
                                lblAnother7.Content = "ТЬ";
                                break;
                            case 10:
                                lblMinFiveThirty.Foreground = Brushes.WhiteSmoke;
                                lblAnother7.Foreground = Brushes.WhiteSmoke;
                                lblMinFiveThirty.Content = "ДЕСЯ";
                                lblAnother7.Content = "ТЬ";
                                break;
                            case 11:
                                lblEleven.Foreground = Brushes.WhiteSmoke;
                                break;
                            case 12: 
                                lblTwelve.Foreground = Brushes.WhiteSmoke;
                                break;
                            default:
                                break;
                        }
                    }
                }
           }));
               
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Window_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }
    }
}
